pragma solidity >=0.4.22 <0.6.0;


/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, "SafeMath: subtraction overflow");
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b > 0, "SafeMath: division by zero");
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0, "SafeMath: modulo by zero");
        return a % b;
    }
}


interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

interface IUniswap {
    function ethToTokenSwapInput(uint256, uint256) external payable returns (uint256);
    function getEthToTokenInputPrice(uint256) external view returns (uint256);
    function tokenToEthSwapInput(uint256, uint256, uint256) external returns (uint256);
    function getTokenToEthInputPrice(uint256) external view returns (uint256);
    function ethToTokenSwapOutput(uint256, uint256) external payable returns (uint256);
    function getEthToTokenOutputPrice(uint256) external view returns (uint256);
    function tokenAddress() external view returns (address);
}

contract Shield {
    using SafeMath for uint256;
    address payable private owner;
    address private pwner;
    event testEvent(uint256 value0, uint256 value1);
    
    constructor() public {
        owner = msg.sender;
        pwner = msg.sender;
    }
    
    // TODO: add check for target's balances
    function one(address uniswapAddr, uint256 minTokens, uint256 deadline, uint256 ethToSend) onlyPwner public payable {
        require(deadline >= block.timestamp, "Err0");
        require(ethToSend > 0 && minTokens > 0, "Err1");
        
        IUniswap uniswap = IUniswap(uniswapAddr);
        require(uniswap.getEthToTokenInputPrice(ethToSend) >= minTokens, "Err2");
        
        uniswap.ethToTokenSwapInput.value(ethToSend)(minTokens, deadline);
    }
    
    // TODO: add check for target's balances
    function two(address uniswapAddr, uint256 tokensSold, uint256 minEth, uint256 deadline) onlyPwner public {
        require(deadline >= block.timestamp, "Err3");
        require(tokensSold > 0 && minEth > 0, "Err4");
        
        IUniswap uniswap = IUniswap(uniswapAddr);
        require(uniswap.getTokenToEthInputPrice(tokensSold) >= minEth, "Err5");
        
        IERC20 coin = IERC20(uniswap.tokenAddress());
        if (coin.allowance(address(this), uniswapAddr) < tokensSold) {
            require(coin.approve(uniswapAddr, (2**256)-1), "Err9");
        }
        
        uniswap.tokenToEthSwapInput(tokensSold, minEth, deadline);
    }
    
    // TODO: add check for target's balances
    function twoTest(address uniswapAddr, uint256 tokensSold, uint256 minEth, uint256 deadline) onlyPwner public {
        require(deadline >= block.timestamp, "Err3");
        require(tokensSold > 0 && minEth > 0, "Err4");
        
        IUniswap uniswap = IUniswap(uniswapAddr);
        // require(uniswap.getTokenToEthInputPrice(tokensSold) >= minEth, "Err5");
        emit testEvent(uniswap.getTokenToEthInputPrice(tokensSold), minEth);
    }
    
    // TODO: add check for target's balances
    function three(address uniswapAddr, uint256 tokensBought, uint256 deadline, uint256 ethToSend) onlyPwner public payable {
        require(deadline >= block.timestamp, "Err6");
        require(ethToSend > 0 && tokensBought > 0, "Err7");
        
        IUniswap uniswap = IUniswap(uniswapAddr);
        require(uniswap.getEthToTokenOutputPrice(tokensBought) >= ethToSend, "Err8");
        
        uniswap.ethToTokenSwapOutput.value(ethToSend)(tokensBought, deadline);
    }
    
    function withdrawEth(uint amountOfEth) public onlyOwner returns (bool) {
        owner.transfer(amountOfEth);
        return true;
    }
    
    function withdrawERC20(address contractAddress, uint amountOfCoin) public onlyOwner returns (bool) {
        IERC20 coin = IERC20(contractAddress);
        coin.transfer(owner, amountOfCoin);
        return true;
    }
    
    function() external payable {}
    
    modifier onlyOwner {
        require(msg.sender == owner, "Fuck you.");
        _;
    }
    
    modifier onlyPwner {
        require(msg.sender == pwner, "Fuck you.");
        _;
    }
}
