import time
import datetime
from web3 import Web3, IPCProvider, WebsocketProvider, HTTPProvider
import logging
import addresses
import uniswapExchangeContractABI
import tokenContractABI
import cloakABI
import math
import traceback
import random


def getInputPrice(Ia, Ir, Or):
    input_amount_with_fee = Ia * 997
    numerator = input_amount_with_fee * Or
    denominator = (Ir * 1000) + input_amount_with_fee
    return int(numerator / denominator)


def getOutputPrice(Oa, Ir, Or):
    numerator = Ir * Oa * 1000
    denominator = (Or - Oa) * 997
    return int(numerator / denominator)


def getInputAmountFromMinOutput(Ir1, Or1, Ia2, Oa2):
    a = 997000 * Oa2
    b = 1997000 * Oa2 * Ir1 + (997 ** 2) * Oa2 * Ia2
    c = (1000 ** 2) * Oa2 * (Ir1 ** 2) + 997000 * Oa2 * Ia2 * Ir1 - 997000 * Or1 * Ia2 * Ir1

    Ia1Pos = int((-b + math.sqrt((b ** 2) - (4 * a * c))) / (2 * a))
    Ia1Neg = int((-b - math.sqrt((b ** 2) - (4 * a * c))) / (2 * a))
    log('Ia1Pos = {}'.format(Ia1Pos), 'info')
    log('Ia1Neg = {}'.format(Ia1Neg), 'info')

    if Ia1Pos <= 0 and Ia1Neg <= 0:
        log('Ia1Pos = {}, Ia1Neg = {}'.format(Ia1Pos, Ia1Neg), 'error')
        return
    if Ia1Pos > 0 and Ia1Neg < 0:
        return Ia1Pos
    if Ia1Neg > 0 and Ia1Pos < 0:
        return Ia1Neg
    # If they're both positive, return the one with the lowest value to be conservative
    if Ia1Neg < Ia1Pos and Ia1Neg > 0:
        return Ia1Neg
    if Ia1Pos < Ia1Neg and Ia1Pos > 0:
        return Ia1Pos
    if Ia1Pos == Ia1Neg:
        return Ia1Pos
    # # All the cases should be covered above but default to what is most likely the + value


# TODO: should be getOutputAmountFromMaxInput
def getOutputAmountFromMinInput(Ir1, Or1, Ia2, Oa2):
    Oa1Pos = int((1988018 * Or1 * Ia2 - 994009 * Ia2 * Oa2 + 3000 * Ir1 * Oa2 + math.sqrt(
        988053892081 * Ia2 * Ia2 * Oa2 * Oa2 - 5964054000 * Ir1 * Ia2 * Oa2 * Oa2 + 3976036000000 * Or1 * Ir1 * Ia2 * Oa2 + 9000000 * Ir1 * Ir1 * Oa2 * Oa2
    )) / (1988018 * Ia2))

    Oa1Neg = int((1988018 * Or1 * Ia2 - 994009 * Ia2 * Oa2 + 3000 * Ir1 * Oa2 - math.sqrt(
        988053892081 * Ia2 * Ia2 * Oa2 * Oa2 - 5964054000 * Ir1 * Ia2 * Oa2 * Oa2 + 3976036000000 * Or1 * Ir1 * Ia2 * Oa2 + 9000000 * Ir1 * Ir1 * Oa2 * Oa2
    )) / (1988018 * Ia2))

    print('Oa1Pos = {}'.format(Oa1Pos))
    # log('Oa1Pos = {}'.format(Oa1Pos), 'info')
    print('Oa1Neg = {}'.format(Oa1Neg))
    # log('Oa1Neg = {}'.format(Oa1Neg), 'info')

    if Oa1Pos <= 0 and Oa1Neg <= 0:
        print('Error. Oa1Pos = {}, Oa1Neg = {}'.format(Oa1Pos, Oa1Neg))
        # log('Oa1Pos = {}, Oa1Neg = {}'.format(Oa1Pos, Oa1Neg), 'error')
        return
    if Oa1Pos > 0 and Oa1Neg <= 0:
        return Oa1Pos
    if Oa1Neg > 0 and Oa1Pos <= 0:
        return Oa1Neg
    # If they're both positive, return the one with the lowest value to be conservative
    if Oa1Neg < Oa1Pos and Oa1Neg > 0:
        return Oa1Neg
    if Oa1Pos < Oa1Neg and Oa1Pos > 0:
        return Oa1Pos
    if Oa1Pos == Oa1Neg:
        return Oa1Pos
    # # All the cases should be covered above but default to what is most likely the + value

def now():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

def log(text, level):
    text = '{}\n'.format(text)
    print(text)
    if level == 'debug':
        logger.debug(text)
    if level == 'info':
        logger.info(text)
    if level == 'warning':
        logger.warning(text)
    if level == 'error':
        logger.error(text)
    if level == 'critical':
        logger.critical(text)

def attriDictToDict(tx):
    cleanTx = {}
    cleanTx['from'] = tx['from']
    cleanTx['to'] = tx['to']
    cleanTx['gas'] = w3.toHex(tx['gas'])
    cleanTx['gasPrice'] = w3.toHex(tx['gasPrice'])
    cleanTx['value'] = w3.toHex(tx['value'])
    cleanTx['data'] = tx['input']
    return cleanTx


def signSendTx(txDict, name):
    signedTx = w3.eth.account.signTransaction(txDict, private_key=walletPrivateKey)
    log('{} = {}'.format(name, txDict), 'info')
    result = w3.eth.sendRawTransaction(signedTx.rawTransaction)
    log('result {} = {}'.format(name, w3.toHex(result)), 'critical')


def addToDictTargetFromAddrToDictTxsToRuns(tx, runs):
    global dictTargetFromAddrToDictTxsToRuns

    if tx['from'] in dictTargetFromAddrToDictTxsToRuns:
        # This shouldn't usually ever be true
        if tx in dictTargetFromAddrToDictTxsToRuns[tx['from']]:
            dictTargetFromAddrToDictTxsToRuns[tx['from']][tx].append(runs)
        else:
            dictTargetFromAddrToDictTxsToRuns[tx['from']][tx] = []
            dictTargetFromAddrToDictTxsToRuns[tx['from']][tx].append(runs)
    else:
        dictTargetFromAddrToDictTxsToRuns[tx['from']] = {tx: []}
        dictTargetFromAddrToDictTxsToRuns[tx['from']][tx].append(runs)

    print(dictTargetFromAddrToDictTxsToRuns)


def checkTraceForUniswap(tx):
    cleantx = attriDictToDict(tx)
    try:
        fullTrace = w3.manager.request_blocking("trace_call", [cleantx, ['trace', 'stateDiff'], "latest"])
    except Exception as e:
        log('Couldn\'t get trace', 'error')
        log('tx = {}'.format(tx), 'error')
        log('Error = {}'.format(e), 'error')
        return

    tokenToTokenSwapOutputCall = None
    for call in fullTrace.trace:
        if 'callType' in call.action:
            # print('a')
            if call.action.callType == 'call' and call.action.to in addresses.uniswapExchangesLower and len(call.action.input) > 10:
                if call.action.to not in fullTrace.stateDiff:
                    log('Exchange {} balance not modified in stateDif. Tx likely reverted.'.format(call.action.to), 'warning')
                    log('tx = {}'.format(tx), 'warning')
                    return

                # This check is likely obsolete as it should be true whenever the above stateDif check is true
                if 'error' in call:
                    log('Error found in tx', 'warning')
                    log('tx = {}'.format(tx), 'warning')
                    print()
                    print(fullTrace.trace)
                    print()
                    return


                if productionMode:
                    exchangeAddress = w3.toChecksumAddress(call.action.to)
                    exchange = exchangeAddressesToContracts[exchangeAddress]
                    decodedInputFull = exchange.decode_function_input(call.action.input)
                    if fullTrace.stateDiff[call.action.to].balance['*']['from'] != fullTrace.stateDiff[call.action.to].balance['*']['to']:
                        for fcnName in supportedFunctions:
                            if fcnName in str(decodedInputFull[0]):
                                if fcnName == 'tokenToTokenSwapOutput':
                                    tokenToTokenSwapOutputCall = call
                                else:
                                    log('', 'info')
                                    return {'tx': tx, 'call': call, 'decodedInputFull': decodedInputFull, 'exchangeAddress': exchangeAddress, 'exchange': exchange, 'tokenToTokenSwapOutputCall': tokenToTokenSwapOutputCall, 'fullTrace': fullTrace}
    return None

def cancelRuns(tx):
    global dictTargetFromAddrToDictTxsToRuns

    if tx['from'] not in dictTargetFromAddrToDictTxsToRuns:
        return

    log('Canceling runs', 'info')
    for pastTargetTx in dictTargetFromAddrToDictTxsToRuns[tx['from']]:
        if pastTargetTx.nonce == tx.nonce:
            for i, run in enumerate(dictTargetFromAddrToDictTxsToRuns[tx['from']]['pastTargetTx']):
                dict = {
                    'from': run['from'],
                    'to': run['from'],
                    'value': 0,
                    'chainId': 1,
                    'gas': gasLimit,
                    'gasPrice': int(run['gasPrice'] * 1.2),
                    'nonce': run['nonce'],
                }
                signSendTx(dict, 'txCancel{}'.format(i))



def searchMempool():
    pendingTxHashes = w3.eth.getFilterChanges(filter.filter_id)
    failedTxLookups = []

    def checkPendingTx(txHash, failedTx):
        try:
            tx = w3.eth.getTransaction(txHash)
        except Exception as e:
            if failedTx:
                log('Couldn\'t do getTransaction on {}. {}'.format(txHash.hex(), e), 'error')
            else:
                failedTxLookups.append(txHash)
            return

        if tx['from'] == walletAddress:
            return

        txAndCall = checkTraceForUniswap(tx)
        # If it's replacing with another trade, don't replace our old runs since they should get replaced by running
        # this new tx. Even if an adversary is replacing their tx with a different trade that's still uniswap, their
        # trade will now come before ours and therefore our prior pending runs will all fail
        if txAndCall:
            analyzeTx(txAndCall['tx'], txAndCall['call'], txAndCall['decodedInputFull'], txAndCall['exchangeAddress'],
                      txAndCall['exchange'], txAndCall['tokenToTokenSwapOutputCall'], txAndCall['fullTrace'])
        else:
            cancelRuns(tx)



    for txHash in pendingTxHashes:
        checkPendingTx(txHash, False)

    for txHash in failedTxLookups:
        checkPendingTx(txHash, True)


def analyzeTx(tx, call, decodedInputFull, exchangeAddress, exchange, tokenToTokenSwapOutputCall, fullTrace):
    log('\n-----Found!-----', 'info')
    log('fullTrace = {}'.format(fullTrace), 'info')
    log('call = {}'.format(call), 'info')
    log('target tx = {}'.format(tx), 'info')
    log('exchangeAddress = {}'.format(exchangeAddress), 'info')
    log('decodedInputFull = {}'.format(decodedInputFull), 'info')

    if tx.to not in addresses.uniswapExchanges:
        log('tx not a regular user tx. Too risky', 'warning')
        return

    gasPrice = tx.gasPrice
    if gasPrice > maxGasPrice:
        log('Tx gas price too high: {}'.format(gasPrice), 'warning')

    decodedInputs = decodedInputFull[1]
    if 'deadline' not in decodedInputs:
        log('deadline not found in decodedInputs', 'info')
        return
    deadline = decodedInputs['deadline']
    timeOfNextBlock = time.time() + 20
    if deadline < timeOfNextBlock:
        log('Error. deadline < time.time(), {} < {}'.format(deadline, timeOfNextBlock), 'warning')
        return

    log('tokenToTokenSwapOutputCall = {}'.format(tokenToTokenSwapOutputCall), 'info')

    if any(fcnName in str(decodedInputFull[0]) for fcnName in ('ethToTokenSwapInput', 'ethToTokenTransferInput')):
        Ir1 = w3.eth.getBalance(exchangeAddress)
        Or1 = exchangeAddressesToERC20Contracts[exchangeAddress].functions.balanceOf(
            exchangeAddress).call()
        Ia2 = w3.toInt(hexstr=call.action.value)
        log('((Ia2 + Ir1) / Ir1): (({} + {}) / {}) = {}'.format(Ia2, Ir1, Ir1, ((Ia2 + Ir1) / Ir1)), 'critical')
        if ((Ia2 + Ir1) / Ir1) <= minSingleRatio:
            log('Target didn\'t spend enough', 'warning')
            return

        maxIa1Available = w3.eth.getBalance(cloakAddress)
        if maxIa1Available <= 0:
            log('Not enough eth to use', 'warning')
            return

        Oa2 = decodedInputs['min_tokens']
        maxIa1 = getInputAmountFromMinOutput(Ir1, Or1, Ia2, Oa2)
        if maxIa1 == None:
            return
        if maxIa1Available < maxIa1:
            Ia1 = maxIa1Available
        else:
            Ia1 = maxIa1
        Ia1 = int(Ia1 * 0.9999999999)
        Oa1 = exchange.functions.getEthToTokenInputPrice(Ia1).call()
        print('Oa1 = {}'.format(Oa1))

        Ir2 = Ir1 + Ia1
        Or2 = Or1 - Oa1
        Oa2 = getInputPrice(Ia2, Ir2, Or2)
        if Oa2 < decodedInputs['min_tokens']:
            log('Oa2 too low, Oa2 = {}, decodedInputs[\'min_tokens\'] = {}'.format(Oa2, decodedInputs['min_tokens']), 'warning')
            return

        Ia3 = Oa1
        Ir3 = Or2 - Oa2
        Or3 = Ir2 + Ia2
        Oa3 = getInputPrice(Ia3, Ir3, Or3)
        print('Oa3 before Oa3 * 0.999... = {}'.format(Oa3))
        if Oa3 < Ia1:
            log('Oa3 too low, Oa3 = {}, Ia1 = {}'.format(Oa3, Ia1), 'warning')
            return

        executeTrade(tx, exchange, Ia1, int(Oa1 * 0.9999999999), 'ethToTokenSwapInput', gasPrice,
                     deadline, Ia3, int(Oa3 * 0.999999999), tx.gas)

    elif any(fcnName in str(decodedInputFull[0]) for fcnName in ('ethToTokenSwapOutput', 'ethToTokenTransferOutput')):
        Ir1 = w3.eth.getBalance(exchangeAddress)
        Or1 = exchangeAddressesToERC20Contracts[exchangeAddress].functions.balanceOf(
            exchangeAddress).call()
        Oa2 = decodedInputs['tokens_bought']
        # maxIa2 = exchange.functions.getEthToTokenOutputPrice(Oa2).call()

        # if 'ethToTokenSwapOutput' in str(decodedInputFull[0]):
        if call.action['from'] in addresses.uniswapExchangesLower and tokenToTokenSwapOutputCall:
            inputExchangeAddress = w3.toChecksumAddress(call.action['from'])
            log('inputExchangeAddress = {}'.format(inputExchangeAddress), 'info')
            originalExchange = exchangeAddressesToContracts[inputExchangeAddress]
            originalTxDecodedInputFull = originalExchange.decode_function_input(tokenToTokenSwapOutputCall.action.input)
            log('originalTxDecodedInputFull = {}'.format(originalTxDecodedInputFull), 'info')
            # Make maxIa2 slightly smaller so that txs in the originating token are unlikely to make the target's tx failed
            # because of their original tokens being worth less eth
            maxIa2 = int(0.999 * originalExchange.functions.getTokenToEthInputPrice(originalTxDecodedInputFull[1]['max_tokens_sold']).call())
        else:
            maxIa2 = w3.toInt(hexstr=call.action.value)
            log('Regular ethToTokenSwapOutput tx', 'info')

        #     TODO: add a check that it's also generally a token-to-token? Also check max_eth_amount
        log('((Oa2 + Or1) / Or1): (({} + {}) / {}) = {}'.format(Oa2, Or1, Or1, ((Oa2 + Or1) / Or1)), 'critical')
        if ((Oa2 + Or1) / Or1) <= minSingleRatio:
            log('Target didn\'t spend enough', 'warning')
            return

        maxIa1Available = w3.eth.getBalance(cloakAddress)
        maxOa1Available = exchange.functions.getEthToTokenInputPrice(maxIa1Available).call()

        maxOa1 = getOutputAmountFromMinInput(Ir1, Or1, maxIa2, Oa2)
        if maxOa1 == None:
            return
        if maxOa1Available < maxOa1:
            Oa1 = maxOa1Available
        else:
            Oa1 = maxOa1
        Oa1 = int(Oa1 * 0.9999999999)
        Ia1 = exchange.functions.getEthToTokenOutputPrice(Oa1).call()

        Ir2 = Ir1 + Ia1
        Or2 = Or1 - Oa1
        Ia2 = getOutputPrice(Oa2, Ir2, Or2)
        log('maxIa1Available = {}'.format(maxIa1Available), 'info')
        log('Ia1 = {}'.format(Ia1), 'info')
        log('Oa1 = {}'.format(Oa1), 'info')
        log('Ir1 = {}'.format(Ir1), 'info')
        log('Or1 = {}'.format(Or1), 'info')
        log('maxIa2 = {}'.format(maxIa2), 'info')
        log('Ia2 = {}'.format(Ia2), 'info')
        log('Oa2 = {}'.format(Oa2), 'info')
        log('w3.toInt(hexstr=call.action.value) = {}'.format(w3.toInt(hexstr=call.action.value)), 'info')

        Ia3 = Oa1
        Ir3 = Or2 - Oa2
        Or3 = Ir2 + Ia2
        Oa3 = getInputPrice(Ia3, Ir3, Or3)
        if Oa3 < Ia1:
            log('Oa3 too low, Oa3 = {}, Ia1 = {}'.format(Oa3, Ia1), 'warning')
            return

        executeTrade(tx, exchange, Ia1, Oa1, 'ethToTokenSwapOutput', gasPrice,
                     deadline, Ia3, int(Oa3 * 0.999999999), tx.gas)


    else:
        log('Not a supported method', 'warning')
        return

    log('maxIa1Available = {}'.format(maxIa1Available), 'info')
    log('Ia1 = {}'.format(Ia1), 'info')
    log('Oa1 = {}'.format(Oa1), 'info')
    log('Ir1 = {}'.format(Ir1), 'info')
    log('Or1 = {}'.format(Or1), 'info')
    log('Ia2 = {}'.format(Ia2), 'info')
    log('Oa2 = {}'.format(Oa2), 'info')
    log('Yaaay!', 'info')
    log('------------------------------------------------------------------', 'info')


def executeTrade(tx, exchange, Ia1, Oa1, txTypeToSend, gasPrice, deadline, Ia3, Oa3, targetGasLimit):
    global timeToCleanUp
    global gasPriceOfMostRecentTx
    log('', 'info')
    Ia1 = int(Ia1)
    Oa1 = int(Oa1)
    deadline = int(deadline)
    nonce = w3.eth.getTransactionCount(walletAddress)



    # TODO: use random gasprice when in production
    # gasPriceToUse1 = int(gasPrice * (1.1 + random.randint(0, 10) / 20))
    # gasPriceToUse1 = int(gasPrice * 1.11)
    gasPriceToUseA = int(gasPrice + 1)
    gasPriceToUseB = gasPrice
    gasPriceToUseD = gasPrice
    gasPriceToUseE = int(gasPrice - 1)
    if gasPriceToUseE < minGasPrice:
        gasPriceToUseE = minGasPrice

    # Should be numOfClones-1 but it's easier to include txD twice within it
    totalWeiTxFee = (gasLimit * (gasPriceToUseA + gasPriceToUseE)) + (gasLimitFailedTx * numOfClones * (gasPriceToUseB + gasPriceToUseD))
    expectedProfit = Oa3 - Ia1
    if productionMode and expectedProfit < totalWeiTxFee:
        log('totalWeiTxFee less than expected profit, totalWeiTxFee = {}, profit = {}'.format(totalWeiTxFee, expectedProfit), 'error')
        return
    gasLimitWithdraw = targetGasLimit if targetGasLimit > gasLimit else gasLimit

    txBs = []
    txDs = []

    if txTypeToSend == 'ethToTokenSwapInput':
        txDictA = cloak.functions.one(
            exchange.address,
            Oa1,
            deadline,
            Ia1
        ).buildTransaction({
            'chainId': 1,
            'gas': gasLimit,
            'gasPrice': gasPriceToUseA,
            'nonce': nonce,
        })
        nonce += 1

        for i in range(numOfClones):
            txBs.append(cloak.functions.one(
                exchange.address,
                Oa1,
                deadline,
                Ia1
            ).buildTransaction({
                'chainId': 1,
                'gas': gasLimit,
                'gasPrice': gasPriceToUseB,
                'nonce': nonce,
            }))
            nonce += 1

        for i in range(numOfClones):
            txDs.append(cloak.functions.two(
                exchange.address,
                Ia3,
                Oa3,
                deadline
            ).buildTransaction({
                'chainId': 1,
                'gas': gasLimitWithdraw,
                'gasPrice': gasPriceToUseD,
                'nonce': nonce,
            }))
            nonce += 1

        txDictE = cloak.functions.two(
            exchange.address,
            Ia3,
            Oa3,
            deadline
        ).buildTransaction({
            'chainId': 1,
            'gas': gasLimitWithdraw,
            'gasPrice': gasPriceToUseE,
            'nonce': nonce,
        })

    elif txTypeToSend == 'ethToTokenSwapOutput':
        txDictA = cloak.functions.three(
            exchange.address,
            Oa1,
            deadline,
            Ia1
        ).buildTransaction({
            'chainId': 1,
            'gas': gasLimit,
            'gasPrice': gasPriceToUseA,
            'nonce': nonce,
        })
        nonce += 1

        for i in range(numOfClones):
            txBs.append(cloak.functions.three(
                exchange.address,
                Oa1,
                deadline,
                Ia1
            ).buildTransaction({
                'chainId': 1,
                'gas': gasLimit,
                'gasPrice': gasPriceToUseB,
                'nonce': nonce,
            }))
            nonce += 1

        for i in range(numOfClones):
            txDs.append(cloak.functions.two(
                exchange.address,
                Ia3,
                Oa3,
                deadline
            ).buildTransaction({
                'chainId': 1,
                'gas': gasLimitWithdraw,
                'gasPrice': gasPriceToUseD,
                'nonce': nonce,
            }))
            nonce += 1

        txDictE = cloak.functions.two(
            exchange.address,
            Ia3,
            Oa3,
            deadline
        ).buildTransaction({
            'chainId': 1,
            'gas': gasLimitWithdraw,
            'gasPrice': gasPriceToUseE,
            'nonce': nonce,
        })


    signSendTx(txDictA, 'txDictA')

    for i, txDict in enumerate(txBs):
        signSendTx(txDict, 'txDictB{}'.format(i))

    for i, txDict in enumerate(txDs):
        signSendTx(txDict, 'txDictD{}'.format(i))

    signSendTx(txDictE, 'txDictE')

    addToDictTargetFromAddrToDictTxsToRuns(tx, [txDictA] + txBs + txDs + [txDictE])

    timeToCleanUp = time.time() + 300
    gasPriceOfMostRecentTx = gasPriceToUseB

    log('Current block number = {}'.format(w3.eth.blockNumber), 'info')
    log('Ia3 = {}'.format(Ia3), 'info')
    log('Oa3 = {}'.format(Oa3), 'info')

def convertLeftoverBalances():
    nonce = w3.eth.getTransactionCount(walletAddress)
    for i, uniAddr in enumerate(addresses.uniswapExchanges):
        tokenBal = exchangeAddressesToERC20Contracts[uniAddr].functions.balanceOf(cloakAddress).call()
        if tokenBal > 0:

            log('Cleaning up leftover balance with {}'.format(uniAddr), 'info')
            txDict = cloak.functions.two(
                uniAddr,
                tokenBal,
                exchangeAddressesToContracts[uniAddr].functions.getTokenToEthInputPrice(tokenBal).call(),
                int(time.time() + 900)
            ).buildTransaction({
                'chainId': 1,
                'gas': gasLimit,
                'gasPrice': gasPriceOfMostRecentTx,
                'nonce': nonce,
            })
            log('txDict = {}'.format(txDict), 'info')
            signSendTx(txDict, 'cleanupTxDict')
            nonce += 1
            time.sleep(5)


try:
    t0 = time.time()
    logFileName = 'uniSprinter.log'
    verboseLogFileName = 'uniSprinterVerbose.log'

    logging.basicConfig(
        filename=verboseLogFileName,
        filemode='a',
        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s     %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=logging.INFO
    )
    logger = logging.getLogger('uniSprinter')
    log('-----------STARTING-----------', 'info')

    IPCPath = '/root/.local/share/io.parity.ethereum/jsonrpc.ipc'
    w3 = Web3(IPCProvider(IPCPath))
    # TODO: WARNING, HAS BEEN USED FROM PARITY MACHINE
    # infura = Web3(WebsocketProvider('REPLACE'))
    filter = w3.eth.filter('pending')
    productionMode = True

    walletPrivateKey = 'REPLACE'
    walletAddress = 'REPLACE'
    walletAddress = w3.toChecksumAddress(walletAddress)
    ethToKeepInWallet = int(0.1 * (10 ** 18))
    minGasPrice = int(1 * (10 ** 9))
    # TODO: no need for max gas price if it's based on profit - gas accounting for failedTxRate
    maxGasPrice = int(50 * (10 ** 9))
    if productionMode:
        minEth = int(0.1 * (10 ** 18))
    else:
        minEth = 0
    minSingleRatio = 1.0032
    # minSingleRatio = 1
    global timeToCleanUp
    timeToCleanUp = time.time()
    global gasPriceOfMostRecentTx
    gasPriceOfMostRecentTx = 5*10**9
    gasLimit = 200000
    gasLimitFailedTx = 35000
    numOfClones = 3
    global dictTargetFromAddrToDictTxsToRuns
    dictTargetFromAddrToDictTxsToRuns = {}

    methodIDHexToText = {'0xf39b5b9b': 'ethToTokenSwapInput'}
    tokenToEthSwapInputMethodIDHex = '0x95e3c50b'
    supportedFunctions = ['ethToTokenSwapInput', 'ethToTokenTransferInput', 'ethToTokenSwapOutput', 'ethToTokenTransferOutput', 'tokenToTokenSwapOutput']
    supportedFunctionsFull = ['ethToTokenSwapInput', '<Function ethToTokenTransferInput(uint256,uint256,address)>', 'ethToTokenSwapOutput', 'ethToTokenTransferOutput', '<Function tokenToTokenSwapOutput(uint256,uint256,uint256,uint256,address)>']

    exchangeAddressesToContracts = {}
    exchangeAddressesToERC20Addresses = {}
    ERC20AddressesToContracts = {}
    exchangeAddressesToERC20Contracts = {}
    ERC20AddressesToExchangeContracts = {}
    for i, uniAddr in enumerate(addresses.uniswapExchanges):
        exchangeAddressesToContracts[uniAddr] = w3.eth.contract(address=uniAddr, abi=uniswapExchangeContractABI.ABI)
        exchangeAddressesToERC20Addresses[uniAddr] = addresses.uniswapTokens[i]
        ERC20AddressesToContracts[addresses.uniswapTokens[i]] = w3.eth.contract(address=addresses.uniswapTokens[i],
                                                                                abi=tokenContractABI.ABI)
        exchangeAddressesToERC20Contracts[uniAddr] = ERC20AddressesToContracts[addresses.uniswapTokens[i]]
        ERC20AddressesToExchangeContracts[addresses.uniswapTokens[i]] = exchangeAddressesToContracts[uniAddr]

    cloakAddress = 'REPLACE'
    cloakAddress = w3.toChecksumAddress(cloakAddress)
    cloak = w3.eth.contract(address=cloakAddress, abi=cloakABI.ABI)


    while True:
        t2 = time.time()
        if time.time() > timeToCleanUp:
            convertLeftoverBalances()
        for i in range(49):
            if w3.eth.getBalance(walletAddress) > ethToKeepInWallet:
                searchMempool()
            # TODO: remove sleep in production
            time.sleep(0.1)
        print(time.time() - t2)

except Exception as e:
    log('Exception occurred: {}'.format(e), 'error')
    log(traceback.format_exc(), 'error')

